import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late double _baslangic = randomDouble();
  late double _bitis = randomDouble();
  double randomDouble() {
    double randomdouble = Random().nextDouble();
    //generate random double within 0.00 - 1.00;
    return double.parse(randomdouble.toStringAsFixed(4));
    //toStringAsFixed will fix decimal length to 4, 0.3454534 = 0.3454
  }

  @override
  void initState() {
    _controller = AnimationController(
      duration: const Duration(milliseconds: 1000),
      vsync: this,
    );

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Şişe Çevirmece"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            RotationTransition(
              turns: Tween(begin: _baslangic, end: _bitis).animate(_controller),
              child: Image.asset('assets/sise.png'),
            ),
            ElevatedButton(
              child: Text("Çevir"),
              onPressed: () {
                _controller.reset();
                _controller.forward();

                _bitis = randomDouble();
                _baslangic = randomDouble();
              },
            ),
            ElevatedButton(
              child: Text("Sıfırla"),
              onPressed: () => _controller.reset(),
            ),
          ],
        ),
      ),
    );
  }
}
